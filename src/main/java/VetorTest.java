import java.util.Scanner;

public class VetorTest {

    public static void main(String[]args){

        Scanner teclado = new Scanner(System.in);

        int [] vet = new int[10];

        int i ;

        //Inserir 10 números
        //Apresentar a soma dos pares
        System.out.println("Entrar com 10 numeros");

        int pares = 0;
        int qtdadepares = 0;

        for (i=0;i<10;i++){
            vet[i] = teclado.nextInt();
            if (vet[i]%2==0){
                pares = pares + vet[i];
                qtdadepares++;
            }
        }

        System.out.println("A soma de todos os numeros + " + "pares é " + pares);
        System.out.println("Foram inseridos:  " + qtdadepares + "  numerospares");

    }
}
